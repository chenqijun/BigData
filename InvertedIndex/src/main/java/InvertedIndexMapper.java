/**
 * Created by chenqijun on 6/24/17.
 */
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class InvertedIndexMapper
        extends Mapper<Object, Text, Text, Text> {
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
        StringTokenizer itr = new StringTokenizer(value.toString());

        Path filepath = ((FileSplit) context.getInputSplit()).getPath();
        String filePathString = filepath.toString();

        while (itr.hasMoreTokens()) {
            String curWord = itr.nextToken().toString();
            curWord = curWord.toLowerCase();
            curWord = curWord.replaceAll("[^a-zA-Z]", "");
            curWord = curWord.trim();
            if (curWord.length() == 0) {
                continue;
            }
            context.write(new Text(curWord), new Text(filePathString));
        }
    }
}
