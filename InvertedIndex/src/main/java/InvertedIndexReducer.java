/**
 * Created by chenqijun on 6/24/17.
 */
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class InvertedIndexReducer
        extends Reducer<Text,Text,Text,Text> {
    public void reduce(Text key, Iterable<Text> values,
                       Context context
    ) throws IOException, InterruptedException {
        int threashold = 10;
        StringBuilder sb = new StringBuilder();

        String prev = null;
        int count = 0;

        for (Text value : values) {
            if (prev == null) {
                prev = value.toString();
                count = 1;
            } else if (prev.equals(value.toString())) {
                count++;
            } else  {
                if (count < threashold) {
                    prev = value.toString();
                    count = 1;
                    continue;
                } else {
                    sb.append(prev + "\t");

                    prev = value.toString();
                    count = 1;
                }
            }
        }

        if (count >= threashold) {
            sb.append(prev + "\t");
        }

        if (!sb.toString().trim().equals("")) {
            context.write(key, new Text(sb.toString()));
        }
    }

}
