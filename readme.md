# Big Data Study

## Hadoop Template

#### IDE
For IDE I used Idea (Intellij), and start with creating a maven project

#### pom.xml

1. add following to the pom.xml

```
<dependencies>
	<dependency>
		<groupId>org.apache.hadoop</groupId>
		<artifactId>hadoop-core</artifactId>
		<version>1.2.1</version>
	</dependency>
	<dependency>
		<groupId>org.apache.hadoop</groupId>
		<artifactId>hadoop-common</artifactId>
		<version>2.7.2</version>
	</dependency>
</dependencies>
```

2. In idea they will help you import the required marven dependencies

#### class Headers(Libraries)

```
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
```

#### Mapper

```

public class WordCountMapper
        extends Mapper<Object, Text, Text, IntWritable> {
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
        StringTokenizer stringTokenizer = new StringTokenizer(value.toString());

        while (stringTokenizer.hasMoreTokens()) {
            String curWord = stringTokenizer
                                .nextToken()
                                .replaceAll("[^a-zA-Z]", "")
                                .toLowerCase()
                                .trim();
            if (curWord.length() > 0) {
                context.write(new Text(curWord), new IntWritable(1));
            }
        }
    }
}

```

#### Reducer

```
public class IntSumReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {
    private IntWritable result = new IntWritable();

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      result.set(sum);
      context.write(key, result);
    }
  }
```

#### Driver

```
public class WordCountDriver {
	
    Configuration conf = new Configuration();

    
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(WordCount.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
}
```

#### sample code to run in hadoop environment
hdfs dfs -mkdir input
hdfs dfs -put input/* input
hdfs dfs -rm -r output
hadoop jar WordCount.jar WordCountDriver input output
