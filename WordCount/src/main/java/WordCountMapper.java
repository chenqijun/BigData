/**
 * Created by chenqijun on 6/24/17.
 */
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCountMapper
        extends Mapper<Object, Text, Text, IntWritable> {
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
        StringTokenizer stringTokenizer = new StringTokenizer(value.toString());

        while (stringTokenizer.hasMoreTokens()) {
            String curWord = stringTokenizer
                                .nextToken()
                                .replaceAll("[^a-zA-Z]", "")
                                .toLowerCase()
                                .trim();
            if (curWord.length() > 0) {
                context.write(new Text(curWord), new IntWritable(1));
            }
        }
    }
}
