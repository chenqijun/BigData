hadoop WordCount application using mapreduce

```
# copy build java class to hadoop machine
jar cf WordCount.jar WordCount*.class # bundled java class to jar file
hdfs dfs -rm -r output
hdfs dfs -mkdir input
hdfs dfs -put input/* input/
hadoop WordCount.jar WordCountDriver input output
